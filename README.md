# php-fileviewer
It's a simple fileviewer in php. It can list directorys, show files and download them. So that's it 4 the moment.


# Config
You can set a password with the `$lock` variable. The password must be SHA512-encrypted. You can comment out the `$lock`-variable to disable the password protection.
If you rename the file don't forget to change the `$thisfile` variable.
The variable `$dir` is the path of the root-directory the filemanager should show.
`$lang` is the variable to change the language. 


# Translation
Feel free to translate this stuff. 

_Languages I support at the moment:_
  * English
  * German
  * Spanish (translation by [@mondanzo](https://github.com/mondanzo))


# TODO
- [x] Fix (don't `include`; use `file_get_contents` instead, you idiot)
- [x] Security patch (u know, unencrypted password -> make some SHA512-STUFF, set variables -> do some `unset()`, don't use GET to send the password)
- [x] new translation engine
- [x] add some extensions (like MD, GITIGNORE, YML, XML, CONF, CFG, CPP, JS, C#, PY, etc.)
- [ ] File Uploader
- [ ] File Editor
- [ ] add some extensions more (like PS1, VBS, MCFUNCTION, SCF, REG, etc.)
- [ ] add CSS
- [ ] add more languages (help is appreciated)
- [ ] add more TODO
- [ ] Thank [@mondanzo](https://github.com/mondanzo) 4 annoy me with security stuff. 
