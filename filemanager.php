<?php

//CONFIG
$dir = "D:";						// Path of the root directory.
$thisfile = "filemanager.php";		// Name of this File.
$lock = "ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413";					// Passoword (SHA512-Encrypted; default is 123456)
$lang = "en";						// Language

//CODE - DO NOT CHANGE
//
//or change only if you know, what you are doing.

session_start();


// TRANSLATION-ENGINE
function load_lang($lang){
	if(!file_exists("./lang/$lang.json")){
		$lang = "en";
	}
	$language = json_decode(file_get_contents("./lang/$lang.json"), true);
	if($language === false){
		$language = json_decode(file_get_contents("./lang/en.json"), true);
	}
	return $language;
}

$language = load_lang($lang);

//END OF TRANSLATION-ENGINE
$loginform = "<p><form action=\"?&#PLHOLDER#\" method=\"POST\">$language[password]: <input type=\"password\" name=\"lock\"></input></form>";
$rootdir = $dir;
$rdlen = strlen($rootdir);
if(isset($_GET['link'])){
	$loginform = str_replace('&', 'link='.$_GET['link'], $loginform);
	if(isset($_GET['playaudio'])){
		$loginform = str_replace('#PLHOLDER#', '&playaudio&', $loginform);
	}else if(isset($_GET['viewimg'])){
		$loginform = str_replace('#PLHOLDER#', '&viewimg&', $loginform);
	}else if(isset($_GET['viewpdf'])){
		$loginform = str_replace('#PLHOLDER#', '&viewpdf&', $loginform);
	}else if(isset($_GET['viewvideo'])){
		$loginform = str_replace('#PLHOLDER#', '&viewvideo&', $loginform);
	}else if(isset($_GET['viewtext'])){
		$loginform = str_replace('#PLHOLDER#', '&viewtext&', $loginform);
	}
	$loginform = $loginform."</form><p>";
}else{
	$loginform = str_replace('#PLHOLDER#', 'list', $loginform);
}
if(isset($_GET['dir'])){
	$loginform = str_replace('&', '&dir='.$_GET['dir']."&", $loginform);
}
if(isset($lock)){
	if(isset($_POST['lock'])){
		$key = hash('sha512', $_POST['lock']);
		if($lock == $key){
			unset($key, $lock);
			$_SESSION['THISISAVALIDSESSION'] = true;
		}else{
			unset($key, $lock);
			echo($loginform);
			die("<p>".$language["err"]->noauth);
		}
	}else if(isset($_SESSION['THISISAVALIDSESSION'])){

	}else{
		echo($loginform);
		die("<p>".$language["err"]["noauth"]);
	}
}

if(isset($_GET['link'])) {
$var_1 = $_GET['link'];
$file = $dir."/".$_GET['dir']."/". $var_1;


$file = str_replace("..","/",$file);

		if(isset($_GET['playaudio'])){
			echo '<audio src="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$_GET['link'].'" autostart="true" loop="false" autoplay="autoplay" controls ></audio>';
			die();
		}else if(isset($_GET['viewimg'])){
			echo '<img src="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$_GET['link'].'" ></img>';
			die();
		} else if(isset($_GET['viewpdf'])){
			header('Content-Type: application/pdf');
			readfile($file);
			die();
		} else if(isset($_GET['viewvideo'])){
			echo '<video src="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$_GET['link'].'" controls>'.$language["err"]["nohtmlvid"].'</video>';
			die();
		}else if(isset($_GET['viewtext'])){
			echo '<pre>';
			echo file_get_contents($rootdir.'/'.$_GET['dir'].'/'.$_GET['link']);
			echo '</pre>';
			die();
		}
	if (file_exists($file)) {
		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.basename($file).'"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		ob_clean();
		flush();
		readfile($file);
		exit;
    }else{
		echo "Error.";
    }
} else if(isset($_GET['list'])){
	$loginform = $loginform."<input type=\"hidden\" name=\"list\">";
	if(isset($_GET['dir'])){
        $dir = $dir."/".$_GET['dir'];
		$dir = str_replace("..","/",$dir);
		$dir = str_replace('//', '/', $dir);
    }
	$loginform = $loginform."</form><p>";
    echo "Viewing: ".$dir."<p>";
    $dh  = opendir($dir);
	if(is_dir($dir) && isset($dh)){
		while (false !== ($filename = readdir($dh))) {
			$files[] = $filename;
		}
	}else{
		echo $language["err"]["dirnotfound"];
	}
    $files = array_diff($files, ["..", "."]);
    sort($files);
	$fn = '';
	for($i = 0+strlen($rootdir); $i <= strlen($dir); $i++){
		$fn = $fn.$dir[$i];
	}
	echo '<a href="'.$thisfile.'?list&dir">[/]</a><br>';
	echo '<a href="'.$thisfile.'?list&dir='.getLink($dir, $fn, $rootdir).'">[..]</a><br>';
    foreach($files as $filename){
		$size = filesize($dir.'/'.$filename);
		$units = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		$power = $size > 0 ? floor(log($size, 1024)) : 0;
		$finfsiz = number_format($size / pow(1024, $power), 2, '.', ',').' '.$units[$power];
		$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
		if(is_dir($dir."/".$filename)) {
			echo '<a href="'.$thisfile.'?list&dir='.$fn.'/'.$filename.'">'.$filename, '</a> - '.$finfsiz.' [DIR]<br>';
		}else if($ext == "mp3"){
			echo '<a href="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$filename.'">'.$filename, '</a> - '.$finfsiz.' - <a href="'.$thisfile.'?dir='.$_GET['dir'].'&playaudio&link='.$filename.'">['.$language["ext"]["audio"].']</a><br>';
		} else if($ext == "png" || $ext == "jpg" || $ext == "jpeg"  || $ext == "ico" || $ext == "gif" || $ext == "svg"){
			echo '<a href="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$filename.'">'.$filename, '</a> - '.$finfsiz.' - <a href="'.$thisfile.'?dir='.$_GET['dir'].'&viewimg&link='.$filename.'">['.$language["ext"]["image"].']</a><br>';
		} else if($ext == "pdf"){
			echo '<a href="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$filename.'">'.$filename, '</a> - '.$finfsiz.' - <a href="'.$thisfile.'?dir='.$_GET['dir'].'&viewpdf&link='.$filename.'">['.$language["ext"]["pdf"].']</a><br>';
		} else if($ext == "mp4" || $ext == "webm" || $ext == "flv" || $ext == "wmv" || $ext == "mov"|| $ext == "avi" || $ext == "mpg" || $ext == "mpeg" || $ext == "m3u8" || $ext == "m4v"){
			echo '<a href="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$filename.'">'.$filename, '</a> - '.$finfsiz.' - <a href="'.$thisfile.'?dir='.$_GET['dir'].'&viewvideo&link='.$filename.'">['.$language["ext"]["video"].']</a><br>';
		} else if($ext == "ini" || $ext == "txt" || $ext == "log" || $ext == "java" || $ext == "php"|| $ext == "htm" || $ext == "html" || $ext == "htaccess" || $ext == "cmd" || $ext == "json" || $ext == "sh" || $ext == "bat" || $ext == "sh" || $ext == "md" || $ext == "gitignore" || $ext == "yml" || $ext == "xml" || $ext == "conf"  || $ext == "cfg" || $ext == "cpp" || $ext == "js" || $ext == "py" || $ext == "sh"){
			echo '<a href="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$filename.'">'.$filename, '</a> - '.$finfsiz.' - <a href="'.$thisfile.'?dir='.$_GET['dir'].'&viewtext&link='.$filename.'">['.str_replace('EXT', strtoupper($ext), $language["ext"]["text"]).']</a><br>';
		} else {
			echo '<a href="'.$thisfile.'?dir='.$_GET['dir'].'&link='.$filename.'">'.$filename.'</a> - '.$finfsiz.' - ['.strtoupper($ext).']<br>';
		}
    }
}

function getLink ($stack, $fn, $rootdir){
	$stack = str_replace($rootdir, '', $stack);
    $arr = array_filter(explode('/',$stack));
    $out = array('/'.implode('/',$arr).'/');
    while((array_pop($arr) and !empty($arr))){
        $out[] = '/'.implode('/',$arr).'/';
    };
	$res = $out[1];
	return $res;
}

?>
